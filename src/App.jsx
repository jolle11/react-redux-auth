import { useEffect } from 'react';
import { HomePage, LoginPage, RegisterPage, ProfilePage } from './pages';
import PrivateRoute from './components/PrivateRoute';
import { Route, Routes, Link } from 'react-router-dom';
import { checkUserSession } from './redux/actions/auth.actions';
import { connect } from 'react-redux';
import './App.scss';

/**
 * TODO:
 *    - LOGOUT
 *    - REFACTOR
 */

const App = ({ dispatch, user }) => {
    useEffect(() => {
        dispatch(checkUserSession());
    }, []);

    return (
        <div className="app">
            <h1>App con autenticación completa</h1>
            <h2>Bienvenido de nuevo {user && user.username}</h2>

            <Link to="/">Ir a la Home</Link>
            <Link to="/register">Ir a Registro</Link>
            <Link to="/login">Ir a Login</Link>
            <Link to="/profile">Ir a Perfil</Link>

            <Routes>
                <Route path="/" element={<HomePage />} />
                <Route path="/register" element={<RegisterPage />} />
                <Route path="/login" element={<LoginPage />} />
                <Route path="/profile" element={<PrivateRoute component={<ProfilePage />} />} />
            </Routes>
        </div>
    );
};

const mapStateToProps = (state) => ({
    user: state.auth.user,
});

export default connect(mapStateToProps)(App);
