import { connect } from 'react-redux';

const ProfilePage = ({ user }) => {
    return (
        <>
            <h1>PERFIL</h1>

            <h2>Nombre: {user.username}</h2>
            <h2>Email: {user.email}</h2>

            <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Minus provident alias laborum mollitia
                explicabo et eligendi ratione dolores cum iusto saepe eum fuga doloribus corrupti illo nesciunt,
                possimus autem rerum?
            </p>
            <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Minus provident alias laborum mollitia
                explicabo et eligendi ratione dolores cum iusto saepe eum fuga doloribus corrupti illo nesciunt,
                possimus autem rerum?
            </p>
            <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Minus provident alias laborum mollitia
                explicabo et eligendi ratione dolores cum iusto saepe eum fuga doloribus corrupti illo nesciunt,
                possimus autem rerum?
            </p>
            <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Minus provident alias laborum mollitia
                explicabo et eligendi ratione dolores cum iusto saepe eum fuga doloribus corrupti illo nesciunt,
                possimus autem rerum?
            </p>
        </>
    );
};

const mapStateToProps = (state) => ({
    user: state.auth.user,
});

export default connect(mapStateToProps)(ProfilePage);
